import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'dart:async';

import 'package:logger/logger.dart';

part 'network_connection_event.dart';

part 'network_connection_state.dart';

part 'network_connection_bloc.freezed.dart';

@Injectable()
class NetworkConnectionBloc extends Bloc<NetworkConnectionEvent, NetworkConnectionState> {
  StreamSubscription ? streamSubscription;
  NetworkConnectionBloc() : super(const NetworkConnectionState.initial()) {
    on<NetworkConnectionEvent>((event, emit) {
      Logger().wtf(event);
      if(event is _$_Started){

      }
      if(event is _$_HasConnection){
        emit(const _$_Connect('it is connected'));
      }
      if(event is _$_Disconnected){
        Fluttertoast.showToast(msg: 'you are offline please check your connection ',backgroundColor: Colors.redAccent);
        emit(const _$_Disconnect('you are offline please check your connection '));
      }

    });
    streamSubscription = Connectivity().onConnectivityChanged.listen((
        ConnectivityResult connectivityResult) {
      if (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.bluetooth){
        add(_$_HasConnection());
      }

      else {
        add(_$_Disconnected());
      }
      @override
      Future<void>close(){
        streamSubscription!.cancel();
        return super.close();
      }
    });
  }
}
