part of 'network_connection_bloc.dart';

@freezed
class NetworkConnectionState with _$NetworkConnectionState {
  const factory NetworkConnectionState.initial() = _Initial;
  const factory NetworkConnectionState.disconnect(String text) = _Disconnect;
  const factory NetworkConnectionState.connect(String text) = _Connect;
}
