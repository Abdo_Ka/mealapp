part of 'network_connection_bloc.dart';

@freezed
class NetworkConnectionEvent with _$NetworkConnectionEvent {
  const factory NetworkConnectionEvent.started() = _Started;
  const factory NetworkConnectionEvent.hasConnection() = _HasConnection;
  const factory NetworkConnectionEvent.disconnected() = _Disconnected;

}
