// To parse this JSON data, do
//
//     final categoryFood = categoryFoodFromJson(jsonString);

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';

part 'category_food.freezed.dart';

part 'category_food.g.dart';

@freezed
class CategoryFoodModel with _$CategoryFoodModel {
  const factory CategoryFoodModel({
    List<Category>? categories,
  }) = _CategoryFoodModel;

  factory CategoryFoodModel.fromJson(Map<String, dynamic> json) =>
      _$CategoryFoodModelFromJson(json);
}

@freezed
class Category with _$Category {
  const factory Category({
    String? idCategory,
    String? strCategory,
    String? strCategoryThumb,
    String? strCategoryDescription,
  }) = _Category;

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);
}
