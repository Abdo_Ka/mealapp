import 'package:dartz/dartz.dart';
import 'package:meal_app/core/error/failers.dart';
import 'package:meal_app/futures/food/domain/model/Get_food_model.dart';

import '../model/category_model/category_food.dart';

abstract class FoodRepositories {
  Future<Either<Failures, GetFoodModel>> getRandomFood();
  Future<Either<Failures, GetFoodModel>> getDetailFood(int id);
  Future<Either<Failures, CategoryFoodModel>> getFoodCategories();
  Future<Either<Failures, GetFoodModel>> getCategoriesType(String type);

}
