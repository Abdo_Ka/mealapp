import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:meal_app/core/error/failers.dart';
import 'package:meal_app/core/usecase.dart';

import '../model/Get_food_model.dart';
import '../repositories/food_repositories.dart';
@Injectable()
class  GetCategoryTypeUseCases extends Usecase<GetFoodModel,String>{
  final FoodRepositories foodRepositories;

  GetCategoryTypeUseCases(this.foodRepositories);
  @override
  Future<Either<Failures, GetFoodModel>> call(String params)=>foodRepositories.getCategoriesType(params);

}