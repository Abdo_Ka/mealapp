import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:meal_app/core/error/failers.dart';
import 'package:meal_app/core/usecase.dart';
import 'package:meal_app/futures/food/domain/repositories/food_repositories.dart';

import '../model/category_model/category_food.dart';

@Injectable()
class GetFoodCatagoriesUseCases
    implements Usecase<CategoryFoodModel, NoParams> {
  final FoodRepositories foodRepositories;

  GetFoodCatagoriesUseCases(this.foodRepositories);

  @override
  Future<Either<Failures, CategoryFoodModel>> call(NoParams params) =>
      foodRepositories.getFoodCategories();
}
