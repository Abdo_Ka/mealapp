import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:meal_app/core/error/failers.dart';
import 'package:meal_app/core/usecase.dart';
import 'package:meal_app/futures/food/domain/model/Get_food_model.dart';
import 'package:meal_app/futures/food/domain/repositories/food_repositories.dart';

@Injectable()
class GetRandomFoodUseCases implements Usecase<GetFoodModel, NoParams> {
  final FoodRepositories foodRepositories;

  GetRandomFoodUseCases(this.foodRepositories);

  @override
  Future<Either<Failures, GetFoodModel>> call(NoParams params) =>
      foodRepositories.getRandomFood();
}
