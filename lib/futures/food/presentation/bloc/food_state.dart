part of 'food_bloc.dart';

@immutable
class FoodState {
  final CommonState<CategoryFoodModel> getCategoriesStatus;
  final CommonState<GetFoodModel> getRandomStatus;
  final CommonState<GetFoodModel>getCategoryType ;
  final CommonState<GetFoodModel>getDetailFood;
  final List<CategoryFoodModel> category;
  final List<GetFoodModel> food;


  FoodState({
    this.getCategoriesStatus = const Initial(),
    this.getRandomStatus = const Initial(),
    this.getCategoryType =const GetCategory(),
    this.getDetailFood= const GetDetail(),
    this.category = const [],
    this.food = const [],
  });
  FoodState copyWith({
    CommonState<CategoryFoodModel>? getCategoriesStatus,
    CommonState<GetFoodModel>? getRandomStatus,
    CommonState<GetFoodModel>? getCategoryType,
    CommonState<GetFoodModel>? getDetailFood,
    List<CategoryFoodModel>? category,
    List<GetFoodModel>? food,
    String ? type,
  }) {
    return FoodState(
      getCategoriesStatus: getCategoriesStatus ?? this.getCategoriesStatus,
      getRandomStatus: getRandomStatus ?? this.getRandomStatus,
      getCategoryType:getCategoryType ?? this.getCategoryType,
      getDetailFood: getDetailFood ?? this.getDetailFood,
      category: category ?? this.category,
      food: food ?? this.food,
    );
  }


}
