import 'package:meal_app/futures/food/domain/model/Get_food_model.dart';
import '../../domain/usecases/get_food_catagories_usecses.dart';
import '../../domain/model/category_model/category_food.dart';
import '../../domain/usecases/get_random_food_usecases.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import '../../domain/usecases/get_detail_usecases.dart';
import '../../domain/usecases/get_catagories_type.dart';
import 'package:meal_app/core/common_state.dart';
import 'package:injectable/injectable.dart';
import 'package:meal_app/core/usecase.dart';
import 'package:bloc/bloc.dart';

part 'food_event.dart';

part 'food_state.dart';

@injectable
class FoodBloc extends Bloc<FoodEvent, FoodState> {
  final GetRandomFoodUseCases getRandomFoodUseCases;
  final GetFoodCatagoriesUseCases getFoodCatagoriesUseCases;
  final GetCategoryTypeUseCases getCategoryTypeUseCases;
  final GetDetailUseCases getDetailUseCases;

  FoodBloc(this.getRandomFoodUseCases, this.getFoodCatagoriesUseCases, this.getCategoryTypeUseCases, this.getDetailUseCases)
      : super(FoodState()) {
    on<Started>(
      (event, emit) async {
        emit(state.copyWith(
            getCategoriesStatus: const Loading(),
            getRandomStatus: const Loading()));

        final randomResult = await getRandomFoodUseCases(NoParams());

        randomResult.fold(
            (l) => emit(state.copyWith(getRandomStatus: Failure(l))),
            (r) => emit(state.copyWith(getRandomStatus: Success(r))));

        final categoryResult = await getFoodCatagoriesUseCases(NoParams());

        categoryResult.fold(
            (l) => emit(state.copyWith(getCategoriesStatus: Failure(l))),
            (r) => emit(state.copyWith(getCategoriesStatus: Success(r))));
      },
    );
    on<GetCategoryType>((event, emit)async {
      emit(state.copyWith(getCategoryType: const Loading()));
      final categoryType= await getCategoryTypeUseCases(event.type);
      categoryType.fold(
              (l)=> emit(state.copyWith(getCategoryType: Failure(l))),
              (r)=>  emit(state.copyWith(getCategoryType: Success(r)))

      );
    });
    on<GetDetailsFood>((event, emit) async{
      emit(state.copyWith(getDetailFood: const Loading()));
      final foodDetail= await getDetailUseCases(event.id);
      foodDetail.fold(
              (l) => emit(state.copyWith(getDetailFood: Failure(l))),
              (r) => emit(state.copyWith(getDetailFood: Success(r)))
      );
    });
  }
}
