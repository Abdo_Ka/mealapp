part of 'food_bloc.dart';

abstract class FoodEvent {}

class Started extends FoodEvent {}

class GetRandomFood extends FoodEvent {}

class GetCategoryType extends FoodEvent {
  final String type;
  GetCategoryType(this.type);
}
class GetDetailsFood extends FoodEvent{
  final int id;

  GetDetailsFood(this.id);
}

class GetCategories extends FoodEvent {}
