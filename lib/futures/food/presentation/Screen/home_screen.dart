import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:logger/logger.dart';
import 'package:meal_app/core/common_state.dart';
import 'package:meal_app/futures/food/domain/model/Get_food_model.dart';
import 'package:meal_app/futures/food/domain/model/category_model/category_food.dart';
import '../../../../core/presentation/widget/shimmer_loading.dart';
import '../bloc/food_bloc.dart';
import 'package:carousel_slider/carousel_slider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<FoodBloc>(context).add(Started());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<FoodBloc, FoodState>(
          builder: (context, state) {
            if (state.getCategoriesStatus is Success &&
                state.getRandomStatus is Success) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CarouselSlider(
                      items: [0, 1, 2, 3, 4].map((i) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              child: CachedNetworkImage(imageUrl: (state.getRandomStatus
                              as Success<GetFoodModel>)
                                  .data!
                                  .meals![0]
                                  .strMealThumb!,
                                placeholder: (context, url) =>
                                const CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                              )
                            );
                          },
                        );
                      }).toList(),
                      options: CarouselOptions(
                        height: 300,
                        aspectRatio: 16 / 9,
                        viewportFraction: 0.8,
                        initialPage: 0,
                        enableInfiniteScroll: true,
                        reverse: false,
                        autoPlay: true,
                        autoPlayInterval: const Duration(seconds: 3),
                        autoPlayAnimationDuration:
                            const Duration(milliseconds: 800),
                        autoPlayCurve: Curves.fastOutSlowIn,
                        enlargeCenterPage: true,
                        enlargeFactor: 0.3,
                        scrollDirection: Axis.horizontal,
                      )),
                  const SizedBox(
                    height: 50,
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Category'),
                            TextButton(
                                onPressed: () {
                                  context.push('/categoryScreen');
                                },
                                child: const Text('View all'))
                          ],
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                            children: List.generate(
                          4,
                          (index) => Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                                width: 150,
                                height: 150,
                                child: InkWell(
                                  onTap: (){
                                    context.push('/categoryType',extra: (state.getCategoriesStatus
                                    as Success<CategoryFoodModel>)
                                        .data!
                                        .categories![index]
                                        .strCategory);
                                  },
                                  child: Card(
                                      child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      CachedNetworkImage(imageUrl:  (state.getCategoriesStatus
                                      as Success<CategoryFoodModel>)
                                          .data!
                                          .categories![index]
                                          .strCategoryThumb!,
                                        width: 100,
                                        height: 100,
                                        placeholder: (context, url) =>
                                        const CircularProgressIndicator(),
                                        errorWidget: (context, url, error) =>
                                        const Icon(Icons.error),
                                      ),
                                      Text((state.getCategoriesStatus
                                              as Success<CategoryFoodModel>)
                                          .data!
                                          .categories![index]
                                          .strCategory!),
                                    ],
                                  )),
                                )),
                          ),
                        )),
                      )
                    ],
                  )
                ],
              );
            } else if (state.getCategoriesStatus is Failure ||
                state.getRandomStatus is Failure) {
              return Text(
                  (state.getRandomStatus as Failure).failure.toString());
            } else if (state.getCategoriesStatus is Loading ||
                state.getRandomStatus is Loading) {
              return IgnorePointer(child: Center(child: SpinKitWanderingCubes(size: 50,color: Colors.black,)));
            }
            return SizedBox();
          },
        ));
  }
}
