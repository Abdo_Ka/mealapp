import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:go_router/go_router.dart';
import 'package:logger/logger.dart';
import 'package:meal_app/core/common_state.dart';
import 'package:meal_app/core/presentation/widget/shimmer_loading.dart';

import '../../domain/model/category_model/category_food.dart';
import '../bloc/food_bloc.dart';

class CategoryScreen extends StatefulWidget {
  const CategoryScreen({super.key});

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

//TODO:
class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<FoodBloc, FoodState>(builder: (context, state) {
          if (state.getCategoriesStatus is Success) {
            return GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                itemCount:
                    (state.getCategoriesStatus as Success<CategoryFoodModel>)
                        .data!
                        .categories!
                        .length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                      width: 150,
                      height: 150,
                      child: InkWell(
                        onTap: (){
                          Logger().d('message');
                          context.push('/categoryType',extra: (state.getCategoriesStatus
                          as Success<CategoryFoodModel>)
                              .data!
                              .categories![index]
                              .strCategory);
                        },
                        child: Card(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CachedNetworkImage(imageUrl: (state.getCategoriesStatus
                            as Success<CategoryFoodModel>)
                                .data!
                                .categories![index]
                                .strCategoryThumb!,
                              width: 100,
                              height: 100,
                              placeholder: (context, url) =>
                              const CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                            ),
                            Text((state.getCategoriesStatus
                                    as Success<CategoryFoodModel>)
                                .data!
                                .categories![index]
                                .strCategory!),
                          ],
                        )),
                      ));
                });
          } else if (state.getCategoriesStatus is Loading) {
            return IgnorePointer(child: Center(child: SpinKitWanderingCubes(size: 50,color: Colors.black)));
          }
          return SizedBox();
        }));
  }
}
