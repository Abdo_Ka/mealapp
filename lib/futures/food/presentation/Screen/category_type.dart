import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:go_router/go_router.dart';
import 'package:logger/logger.dart';
import 'package:meal_app/core/presentation/widget/shimmer_loading.dart';
import 'package:meal_app/futures/food/domain/model/Get_food_model.dart';
import 'package:meal_app/futures/food/presentation/bloc/food_bloc.dart';

import '../../../../core/common_state.dart';

class CategoryType extends StatefulWidget {
  CategoryType({super.key, required this.data});

  final String data;

  @override
  State<CategoryType> createState() => _CategoryTypeState();
}

class _CategoryTypeState extends State<CategoryType> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<FoodBloc>(context).add(GetCategoryType(widget.data));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BlocBuilder<FoodBloc, FoodState>(
        builder: (context, state) {
          if (state.getCategoryType is Success) {
            return GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                itemCount: (state.getCategoryType as Success<GetFoodModel>)
                    .data!
                    .meals!
                    .length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                      width: 150,
                      height: 150,
                      child: InkWell(
                        onTap: () {
                          context.push('/detailFood',
                              extra: (int.parse((state.getCategoryType
                              as Success<GetFoodModel>)
                              .data!
                              .meals![index]
                              .idMeal!)));
                        },
                        child: Card(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CachedNetworkImage(
                              imageUrl: (state.getCategoryType
                                      as Success<GetFoodModel>)
                                  .data!
                                  .meals![index]
                                  .strMealThumb!,
                              width: 100,
                              height: 100,
                              placeholder: (context, url) =>
                                  const CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  const Icon(Icons.error),
                            ),
                            Text(
                                (state.getCategoryType as Success<GetFoodModel>)
                                    .data!
                                    .meals![index]
                                    .strMeal!),
                          ],
                        )),
                      ));
                });
          } else if (state.getCategoryType is Loading) {
            return IgnorePointer(child: Center(child: SpinKitWanderingCubes(size: 50,color: Colors.black)));
          } else
            return SizedBox();
        },
      ),
    );
  }
}
