import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:readmore/readmore.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:meal_app/core/common_state.dart';
import 'package:meal_app/core/presentation/widget/shimmer_loading.dart';
import 'package:meal_app/futures/food/domain/model/Get_food_model.dart';
import 'package:meal_app/futures/food/presentation/bloc/food_bloc.dart';

class DetailsScreen extends StatefulWidget {
  const DetailsScreen({super.key, required this.id});

  final int id;

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<FoodBloc>(context).add(GetDetailsFood(widget.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BlocBuilder<FoodBloc, FoodState>(
        builder: (context, state) {
          if (state.getDetailFood is Success) {
            return SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 300,
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    child: CachedNetworkImage(
                      imageUrl: (state.getDetailFood as Success<GetFoodModel>)
                          .data!
                          .meals![0]
                          .strMealThumb!,
                      placeholder: (context, url) =>
                          const CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Text(
                      'Instructions',
                      style: TextStyle(color: Colors.purple),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ReadMoreText(
                      (state.getDetailFood as Success<GetFoodModel>)
                          .data!
                          .meals![0]
                          .strInstructions!,
                      trimLines: 2,
                      trimMode: TrimMode.Line,
                      trimCollapsedText: 'Show more',
                      trimExpandedText: 'Show less....',
                      moreStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.purple),
                    ),
                  ),
                  Center(
                    child: TextButton(
                        onPressed: () async {
                          await launchUrl(
                              Uri.parse(
                                  (state.getDetailFood as Success<GetFoodModel>)
                                      .data!
                                      .meals![0]
                                      .strYoutube!),
                              mode: LaunchMode.externalApplication);
                        },
                        child: const Text('Show How can I make it on Youtube !!')),
                  )
                ],
              ),
            );
          } else if (state.getDetailFood is Loading) {
            return IgnorePointer(child: Center(child: SpinKitWanderingCubes(size: 50,color: Colors.black,)));
          }
          return Container();
        },
      ),
    );
  }
}
