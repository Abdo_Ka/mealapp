import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';

import '../../../../core/api/constant.dart';
import '../../../../core/connectivity_checker/connectivity_checker.dart';
import '../../../../core/error/errors.dart';
import '../../domain/model/Get_food_model.dart';
import 'package:http/http.dart' as http;

import '../../domain/model/category_model/category_food.dart';

abstract class RemoteDataSource {
  Future<GetFoodModel> getRandomFood();
  Future<GetFoodModel> getDetailFoodDataSource(int id);
  Future<GetFoodModel> getCategoryType(String type);
  Future<CategoryFoodModel> getAllCategory();
}

@Singleton(as: RemoteDataSource)
class ReomteDataSourceWithHttp implements RemoteDataSource {
  final http.Client client;
  final ConnectivityChecker connectivityChecker;

  ReomteDataSourceWithHttp(this.client, this.connectivityChecker);

  @override
  Future<CategoryFoodModel> getAllCategory() async {
    if (await connectivityChecker.hasConnection) {
      final response = await client
          .get(Uri.parse('${Constatnt.baseUrl}/${Constatnt.allCategory}'));

      if (response.statusCode == 200) {
        final CategoryFoodModel get_food_model =
            CategoryFoodModel.fromJson(jsonDecode(response.body));
        return get_food_model;
      } else {
        throw ServerException();
      }
    } else {
      throw OfflineException();
    }
  }

  @override
  Future<GetFoodModel> getRandomFood() async {
    if (await connectivityChecker.hasConnection) {
      final response = await client
          .get(Uri.parse('${Constatnt.baseUrl}/${Constatnt.lookupRandom}'));

      if (response.statusCode == 200) {
        final GetFoodModel get_food_model =
            GetFoodModel.fromJson(jsonDecode(response.body));
        return get_food_model;
      } else {
        throw ServerException();
      }
    } else {
      throw OfflineException();
    }
  }

  @override
  Future<GetFoodModel> getCategoryType(String type) async {
    if (await connectivityChecker.hasConnection) {
      final response = await client
          .get(Uri.parse('${Constatnt.baseUrl}/${Constatnt.filterCategory}$type'));

      if (response.statusCode == 200) {
        final GetFoodModel getCategoryType =
        GetFoodModel.fromJson(jsonDecode(response.body));
        return getCategoryType;
      } else {
        throw ServerException();
      }
    } else {
      throw OfflineException();
    }
  }

  @override
  Future<GetFoodModel> getDetailFoodDataSource(int id) async {
    if (await connectivityChecker.hasConnection) {
      final response = await client
          .get(Uri.parse('${Constatnt.baseUrl}/${Constatnt.lookup}$id'));

      if (response.statusCode == 200) {
        final GetFoodModel getDetailFoodResponse =
        GetFoodModel.fromJson(jsonDecode(response.body));
        return getDetailFoodResponse;
      } else {
        throw ServerException();
      }
    } else {
      throw OfflineException();
    }
  }
}
