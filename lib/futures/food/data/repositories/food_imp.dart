import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';
import 'package:meal_app/core/error/failers.dart';
import 'package:meal_app/futures/food/data/datasources/remotDataSource.dart';
import 'package:meal_app/futures/food/domain/model/Get_food_model.dart';
import 'package:meal_app/futures/food/domain/repositories/food_repositories.dart';

import '../../../../core/error/check_type_failer.dart';
import '../../domain/model/category_model/category_food.dart';

@Injectable(as: FoodRepositories)
class FoodImplement implements FoodRepositories {
  final RemoteDataSource remoteDataSource;

  FoodImplement(this.remoteDataSource);

  @override
  Future<Either<Failures, CategoryFoodModel>> getFoodCategories() async {
    try {
      final categoryFood = await remoteDataSource.getAllCategory();
      return Right(categoryFood);
    } catch (exception) {
      return Left(failuresTypes(exception));
    }
  }

  @override
  Future<Either<Failures, GetFoodModel>> getRandomFood() async {
    try {
      final randomFood = await remoteDataSource.getRandomFood();
      return Right(randomFood);
    } catch (exception) {
      return Left(failuresTypes(exception));
    }
  }

  @override
  Future<Either<Failures, GetFoodModel>> getCategoriesType(String type) async{
    try{
      final categoryType= await remoteDataSource.getCategoryType(type);
      return Right(categoryType);
    }catch(exception){
      return Left(failuresTypes(exception));
    }

  }

  @override
  Future<Either<Failures, GetFoodModel>> getDetailFood(int id) async{
    try{
      final detailFood= await remoteDataSource.getDetailFoodDataSource(id);
      return Right(detailFood);
    }catch(exception){
      return Left(failuresTypes(exception));
    }
  }
}
