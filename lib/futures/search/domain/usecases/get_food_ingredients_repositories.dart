import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:meal_app/core/error/failers.dart';
import 'package:meal_app/core/usecase.dart';

import 'package:meal_app/futures/search/domain/repositories/search_repositories.dart';

import '../model/Get_food_model.dart';

@Injectable()
class GetFoodIngeredientsRepositories
    implements Usecase<GetFoodModel, NoParams> {
  final SearchRepositories searchRepositories;

  GetFoodIngeredientsRepositories(this.searchRepositories);

  @override
  Future<Either<Failures, GetFoodModel>> call(NoParams params) =>
      searchRepositories.getFoodIngredients();
}
