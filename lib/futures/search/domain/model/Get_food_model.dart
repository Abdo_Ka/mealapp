import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'Get_food_model.freezed.dart';

part 'Get_food_model.g.dart';

GetFoodModel getFoodModelFromJson(String str) =>
    GetFoodModel.fromJson(json.decode(str));

String getFoodModelToJson(GetFoodModel data) => json.encode(data.toJson());

@freezed
class GetFoodModel with _$GetFoodModel {
  factory GetFoodModel({
    List<Meal>? meals,
  }) = _GetFoodModel;

  factory GetFoodModel.fromJson(Map<String, dynamic> json) =>
      _$GetFoodModelFromJson(json);
}

@freezed
class Meal with _$Meal {
  factory Meal({
    String? idMeal,
    String? strMeal,
    dynamic? strDrinkAlternate,
    String? strCategory,
    String? strArea,
    String? strInstructions,
    String? strMealThumb,
    String? strTags,
    String? strYoutube,
    String? strIngredient1,
    String? strIngredient2,
    String? strIngredient3,
    String? strIngredient4,
    String? strIngredient5,
    String? strIngredient6,
    String? strIngredient7,
    String? strIngredient8,
    String? strIngredient9,
    String? strIngredient10,
    String? strIngredient11,
    String? strIngredient12,
    String? strIngredient13,
    String? strIngredient14,
    String? strIngredient15,
    dynamic? strIngredient16,
    dynamic? strIngredient17,
    dynamic? strIngredient18,
    dynamic? strIngredient19,
    dynamic? strIngredient20,
    String? strMeasure1,
    String? strMeasure2,
    String? strMeasure3,
    String? strMeasure4,
    String? strMeasure5,
    String? strMeasure6,
    String? strMeasure7,
    String? strMeasure8,
    String? strMeasure9,
    String? strMeasure10,
    String? strMeasure11,
    String? strMeasure12,
    String? strMeasure13,
    String? strMeasure14,
    String? strMeasure15,
    dynamic? strMeasure16,
    dynamic? strMeasure17,
    dynamic? strMeasure18,
    dynamic? strMeasure19,
    dynamic? strMeasure20,
    dynamic? strSource,
    dynamic? strImageSource,
    dynamic? strCreativeCommonsConfirmed,
    dynamic? dateModified,
  }) = _Meal;

  factory Meal.fromJson(Map<String, dynamic> json) => _$MealFromJson(json);
}
