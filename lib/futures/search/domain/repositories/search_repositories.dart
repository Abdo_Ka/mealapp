import 'package:dartz/dartz.dart';
import '../../../../core/error/failers.dart';
import '../model/Get_food_model.dart';

abstract class SearchRepositories {
  Future<Either<Failures, GetFoodModel>> getFoodFirstLetter();

  Future<Either<Failures, GetFoodModel>> getFoodIngredients();

  Future<Either<Failures, GetFoodModel>> getFoodByArea();
}
