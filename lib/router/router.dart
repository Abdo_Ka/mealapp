import 'package:go_router/go_router.dart';
import '../futures/food/presentation/Screen/category_screen.dart';
import '../futures/food/presentation/Screen/category_type.dart';
import '../futures/food/presentation/Screen/details_screen.dart';
import '../futures/food/presentation/Screen/home_screen.dart';
GoRouter router=GoRouter(
  routes: [
    GoRoute(
        path: '/',
      builder: (context,state)=>const HomeScreen(),

    ),
    GoRoute(
        path: '/categoryScreen',
      builder: (context,state)=>const CategoryScreen()
    ),
    GoRoute(
        path: '/categoryType',
        builder: (context,state)=>CategoryType(data: state.extra.toString(),)
    ),
    GoRoute(
        path: '/detailFood',
        builder: (context,state)=>DetailsScreen(id: (state.extra)as int )
    ),
  ]

);