import 'package:logger/logger.dart';
import 'package:meal_app/core/error/errors.dart';

import 'failers.dart';

Failures failuresTypes(Object failure){

  switch (failure.runtimeType){
    case ServerException||ServerFailures:
      return ServerFailures();
    case OfflineFailures||OfflineException :
      return OfflineFailures();
    default:
      return UnknownFailures();
  }
}