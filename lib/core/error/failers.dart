import 'package:equatable/equatable.dart';

abstract class Failures extends Equatable{
  @override
  List<Object?> get props => [];
}
class OfflineFailures extends Failures {}

class UnknownFailures extends Failures {}

class PermissionFailures extends Failures{}

class ServerFailures extends Failures {}

class DataDuplicatesFailures extends Failures {}

class MissingFailures extends Failures {}

class UserNotAllowedToAccessFailures extends Failures {}

class OperationFailedFailures extends Failures {}

class TokenMisMatchFailures extends Failures {}

class DataNotFoundFailures extends Failures {}

class InvalidEmailFailures extends Failures {}

class InvalidPhoneFailures extends Failures {}

class TimeOutFailures extends Failures{}

class NotAuthenticatedFailures extends Failures{}
