
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:injectable/injectable.dart';
@injectable
class ConnectivityChecker{
  final Connectivity _connectivity;

  ConnectivityChecker(this._connectivity);

  Future<bool> get hasConnection async{
    final result = await _connectivity.checkConnectivity();
    return result == ConnectivityResult.mobile || result == ConnectivityResult.wifi;
  }


}
