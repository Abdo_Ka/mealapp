import 'package:meal_app/core/error/failers.dart';

abstract class CommonState<T> {
  const CommonState();
}

class Success<T> extends CommonState<T> {
  final T? data;
  const Success(this.data);
}

class Failure<T> extends CommonState<T> {
  final Failures failure;
  const Failure(this.failure);
}

class Loading<T> extends CommonState<T> {
  const Loading();
}

class Initial<T> extends CommonState<T> {
  const Initial();
}
class GetCategory<T> extends CommonState<T> {
  const GetCategory();
}
class GetDetail<T> extends CommonState<T> {
  const GetDetail();
}