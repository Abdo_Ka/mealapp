
import 'status_code.dart';
import '../error/errors.dart';

Exception get_exception({required int statusCode}) {
  if (statusCode == 330) {
    return DataDuplicatesException();
  } else if (statusCode == StatusCodeType.missingParam.code) {
    return MissingParamException();
  } else if (statusCode == StatusCodeType.notAuthenticated.code) {
    return NotAuthenticatedfaileres();
  } else if (statusCode == StatusCodeType.notAuthorized.code ||
      statusCode == 550) {
    return UserNotAllowedToAccessException();
  } else if (statusCode == StatusCodeType.operationFailed.code) {
    return OperationFailedException();
  } else if (statusCode == 499) {
    return TokenMisMatchException ();
  } else if (statusCode == StatusCodeType.dataNotFound.code) {
    return DataNotFoundException();
  } else if (statusCode == 522) {
    return InvalidEmailException();
  } else if (statusCode == StatusCodeType.invalidPhoneNumber.code) {
    return InvalidPhoneException();
  } else {
    return ServerException();
  }
}