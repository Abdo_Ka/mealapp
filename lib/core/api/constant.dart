class Constatnt{
  static const baseUrl              ="https://www.themealdb.com/api/json/v1/1";

  static const imageThumb           ="https://www.themealdb.com/images/ingredients/";

  static const allCategory          ="categories.php";

  static const lookupRandom         ="random.php";
  static const lookup                ="lookup.php?i=";
  static const search                ="search.php?s=";

  static const category              ="list.php?c=list";
  static const area                  ="list.php?a=list";
  static const ingredients           ="list.php?i=list";

  static const filterCategory       ="filter.php?c=";
  static const filterArea           ="filter.php?a=";
  static const filterIngredients    ="filter.php?i=";
}