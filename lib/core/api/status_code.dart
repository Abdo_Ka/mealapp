import 'package:flutter/material.dart';

class StatusCodeType{

  final int? code;
  const StatusCodeType._({@required this.code});

  // **** Success ****

  static const succeedOperation       = StatusCodeType._(code: 200);
  static const succeedGet             = StatusCodeType._(code: 220);
  static const succeedCreatedPost     = StatusCodeType._(code: 230);
  static const succeedInsertedPost    = StatusCodeType._(code: 220);
  static const succeedPut             = StatusCodeType._(code: 235);
  static const succeedDelete          = StatusCodeType._(code: 250);

  // **** Error 400 ****

  static const missingParam           = StatusCodeType._(code: 400);
  static const unauthorized           = StatusCodeType._(code: 401);
  static const notFound               = StatusCodeType._(code: 404);
  static const httpMethodNotSupported = StatusCodeType._(code: 405);
  static const dataNotFound           = StatusCodeType._(code: 410);
  static const alreadyExist           = StatusCodeType._(code: 420);
  static const notAuthenticated       = StatusCodeType._(code: 430);
  static const notAuthorized          = StatusCodeType._(code: 440);
  static const invalidPhoneNumber     = StatusCodeType._(code: 450);
  static const invalidData            = StatusCodeType._(code: 460);

  // **** Error 500 ****

  static const internalServerError    = StatusCodeType._(code: 500);
  static const handshakeError         = StatusCodeType._(code: 503);
  static const operationFailed        = StatusCodeType._(code: 510);
  static const creatingFail           = StatusCodeType._(code: 520);
  static const insertingFail          = StatusCodeType._(code: 530);
  static const updatingFail           = StatusCodeType._(code: 540);
  static const deletingFail           = StatusCodeType._(code: 550);
  static const serverError            = StatusCodeType._(code: 560);
}