import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';
import 'package:meal_app/core/depenency_injection.config.dart';



//import 'depenency_injection.config.dart';

final GetIt getit = GetIt.instance;

@InjectableInit()
Future<void> init() async {
  getit.init();
}

@module
abstract class RegisterModule {
  Connectivity get connectivity => Connectivity();
  http.Client get client => http.Client();
}
