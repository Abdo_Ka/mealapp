// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:connectivity_plus/connectivity_plus.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:http/http.dart' as _i3;
import 'package:injectable/injectable.dart' as _i2;
import 'package:meal_app/Config/Bloc/network_connection/network_connection_bloc.dart'
    as _i10;
import 'package:meal_app/core/connectivity_checker/connectivity_checker.dart'
    as _i5;
import 'package:meal_app/core/depenency_injection.dart' as _i19;
import 'package:meal_app/futures/food/data/datasources/remotDataSource.dart'
    as _i11;
import 'package:meal_app/futures/food/data/repositories/food_imp.dart' as _i13;
import 'package:meal_app/futures/food/domain/repositories/food_repositories.dart'
    as _i12;
import 'package:meal_app/futures/food/domain/usecases/get_catagories_type.dart'
    as _i14;
import 'package:meal_app/futures/food/domain/usecases/get_detail_usecases.dart'
    as _i15;
import 'package:meal_app/futures/food/domain/usecases/get_food_catagories_usecses.dart'
    as _i16;
import 'package:meal_app/futures/food/domain/usecases/get_random_food_usecases.dart'
    as _i17;
import 'package:meal_app/futures/food/presentation/bloc/food_bloc.dart' as _i18;
import 'package:meal_app/futures/search/domain/repositories/search_repositories.dart'
    as _i7;
import 'package:meal_app/futures/search/domain/usecases/get_food_by_area_repositories.dart'
    as _i6;
import 'package:meal_app/futures/search/domain/usecases/get_food_ingredients_repositories.dart'
    as _i9;
import 'package:meal_app/futures/search/domain/usecases/get_food_letter_repositories.dart'
    as _i8;

extension GetItInjectableX on _i1.GetIt {
  // initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final registerModule = _$RegisterModule();
    gh.factory<_i3.Client>(() => registerModule.client);
    gh.factory<_i4.Connectivity>(() => registerModule.connectivity);
    gh.factory<_i5.ConnectivityChecker>(
        () => _i5.ConnectivityChecker(gh<_i4.Connectivity>()));
    gh.factory<_i6.GetFoodByAreaRepositories>(
        () => _i6.GetFoodByAreaRepositories(gh<_i7.SearchRepositories>()));
    gh.factory<_i8.GetFoodFirstLetterRepositories>(
        () => _i8.GetFoodFirstLetterRepositories(gh<_i7.SearchRepositories>()));
    gh.factory<_i9.GetFoodIngeredientsRepositories>(() =>
        _i9.GetFoodIngeredientsRepositories(gh<_i7.SearchRepositories>()));
    gh.factory<_i10.NetworkConnectionBloc>(() => _i10.NetworkConnectionBloc());
    gh.singleton<_i11.RemoteDataSource>(_i11.ReomteDataSourceWithHttp(
      gh<_i3.Client>(),
      gh<_i5.ConnectivityChecker>(),
    ));
    gh.factory<_i12.FoodRepositories>(
        () => _i13.FoodImplement(gh<_i11.RemoteDataSource>()));
    gh.factory<_i14.GetCategoryTypeUseCases>(
        () => _i14.GetCategoryTypeUseCases(gh<_i12.FoodRepositories>()));
    gh.factory<_i15.GetDetailUseCases>(
        () => _i15.GetDetailUseCases(gh<_i12.FoodRepositories>()));
    gh.factory<_i16.GetFoodCatagoriesUseCases>(
        () => _i16.GetFoodCatagoriesUseCases(gh<_i12.FoodRepositories>()));
    gh.factory<_i17.GetRandomFoodUseCases>(
        () => _i17.GetRandomFoodUseCases(gh<_i12.FoodRepositories>()));
    gh.factory<_i18.FoodBloc>(() => _i18.FoodBloc(
          gh<_i17.GetRandomFoodUseCases>(),
          gh<_i16.GetFoodCatagoriesUseCases>(),
          gh<_i14.GetCategoryTypeUseCases>(),
          gh<_i15.GetDetailUseCases>(),
        ));
    return this;
  }
}

class _$RegisterModule extends _i19.RegisterModule {}
