import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

showMessage(
    String message, {
      bool hasError = true,
      Toast timeShowing = Toast.LENGTH_LONG,
    }) {
  Fluttertoast.cancel().then((value) => Fluttertoast.showToast(
    msg: message,
    backgroundColor: hasError ? Colors.red : Colors.blueAccent,
    textColor: Colors.white,
    fontSize: 16,
    toastLength: timeShowing,
    gravity: ToastGravity.BOTTOM,
  ));
}
