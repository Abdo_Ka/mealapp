import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class Shimmerloading extends StatelessWidget {
  const Shimmerloading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          children:
          List.generate(10, (index) =>Padding(
            padding: const EdgeInsets.only(left:10 ),
            child: Shimmer.fromColors(
              baseColor: Colors.grey,
              highlightColor: Colors.white,
              child: Column(
                children: [
                  Container(
                    width: 250,
                    height: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(16)),color: Colors.grey,),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    width: 250,
                    height: 10,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(16)),color: Colors.grey,),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    width: 250,
                    height: 10,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(16)),color: Colors.grey,),
                  )
                ],
              ),
            ),
          )
            ,
          ) ),
    );
  }
}
