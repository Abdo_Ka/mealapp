import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class shimmer_search extends StatelessWidget {
  const shimmer_search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(),
      body: Column(
        children:List.generate(4, (index) =>   Shimmer.fromColors(
          baseColor: Colors.grey,
          highlightColor: Colors.white,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Container(
                      width: 100,
                      height: 100,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(16)),color: Colors.grey,),
                    ),
                    const SizedBox(width: 20,),
                    Column(
                      children: [
                        Container(
                          width: 200,
                          height: 30,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(16)),color: Colors.grey,),
                        ),
                        const SizedBox(height: 30,),
                        Container(
                          width: 200,
                          height: 30,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(16)),color: Colors.grey,),
                        ),

                      ],
                    ),

                  ],
                ),
              ),
            ],
          ),
        ))
      ),
    );
  }
}
