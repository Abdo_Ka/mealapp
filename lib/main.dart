
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:meal_app/futures/food/presentation/Screen/home_screen.dart';
import 'package:meal_app/futures/food/presentation/bloc/food_bloc.dart';
import 'package:meal_app/router/router.dart';

import 'Config/Bloc/network_connection/network_connection_bloc.dart';
import 'core/depenency_injection.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await init();

  runApp(const BasicApp());
}

class BasicApp extends StatelessWidget {
  const BasicApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return
      MultiBlocProvider(
          providers: [
            BlocProvider(create: (_) => getit<FoodBloc>()),
            BlocProvider(create: (_)=>getit<NetworkConnectionBloc>())
          ],

          child:
          MaterialApp.router(

            title: 'meal app',
            theme: ThemeData(useMaterial3: true),
            debugShowCheckedModeBanner: false,
            routerConfig:  router,

          ));
  }

}
